from django.shortcuts import render, redirect
from django.contrib import messages, auth
from django.contrib.auth.models import User
from contacts.models import Contact


def register(request):
    if request.method == 'POST':
        # GET DATA
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        password2 = request.POST['password2']

        # Validation Data
        if password == password2:
            # check username
            if User.objects.filter(username=username).exists():
                messages.error(request, 'That Username is taken')
                return redirect('register')
            else:
                # check email
                if User.objects.filter(email=email).exists():
                    messages.error(request, 'Email is being used')
                    return redirect('register')
                else:
                    # Looks Good :)
                    new_user = User.objects.create_user(username=username,
                                                        email=email,
                                                        password=password,
                                                        first_name=first_name,
                                                        last_name=last_name)
                    # Automatically lgin after register
                    # auth.login(request, new_user)
                    # return redirect('/')
                    new_user.save()
                    messages.success(request, 'Successfully registered')
                    return redirect('login')

        else:
            messages.error(request, 'Passwords Not Match!')
            return redirect('register')
    else:
        return render(request, 'accounts/register.html')


def login(request):
    if request.method == 'POST':
        # Login User
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(request, username=username, password=password)
        if user is not None:
            auth.login(request, user)
            messages.success(request, 'You are logged in successfully.')
            return redirect('dashboard')
        else:
            messages.error(request, 'Invalid Credentials')
            return redirect('login')
    else:
        return render(request, 'accounts/login.html')


def dashboard(request):
    contacts = Contact.objects.order_by('-contact_date').filter(user_id=request.user.id)
    context = {
        'contacts': contacts,
    }
    return render(request, 'accounts/dashboard.html', context)


def logout(request):
    if request.method == 'POST':
        auth.logout(request)
        messages.success(request, 'Logged out successfully!')

        return redirect('index')